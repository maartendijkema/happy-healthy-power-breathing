---
layout: post
title:  "Happy Healty Power Breathing"
date:   2021-07-12 19:23:55 -0300
categories: breathing hpp
---

Een introductie voor het oefenen in het vertragen van de ademhaling
Het algemene idee bij ademhaling is dat deze natuurlijk - als vanzelf, mag gaan (het gaat, let maar op!)
Een ander algemeen idee is, dat als we merken dat als we niet geheel ontspannen tot en met geagiteerd zijn, we daar best iets aan kunnen doen.
Simpelweg vertraag je ademhaling, dat is eigenlijk alles. En opmerkzaam zijn. Voelen wat er gebeurt.


Er zijn diverse technieken om dit te doen. Deze pagina bespreekt de Happy Healthy Power Breathing techniek.
Voordat die verder wordt toegelicht is het van belang te benadrukken dat de techniek een kordate en zeer effectieve manier is
om het lichaam in een trager ritme te brengen. Daar hoort ook bij dat de techniek te kordaat kan zijn.
Alles wat helpt om te vertragen is mooi meegenomen, hoe het ook voor jou werkt. Grasduin in de overvloed van informatie die beschikbaar is,
en in de eerste plaats je eigen bron van informatie: jouw lichaam.

De motivatie voor het opstellen van deze introductie bestaat erin de informatie die mij via verschilende kanelen ter beschikking is gekomen te bundelen,
en als een verzameling met enige samenhang, perspectief en relativering aan te kunnen bieden.







Happy Healty Power Breathing
============================

Het ritme wordt omlaag gebracht en door het vasthouden van de adem, wordt het lichaam 
gestimuleerd om efficienter te ademen (door beter de longen te vullen, meer bloedlichamen aan te maken).

In de eerste fasen van de beoefening is de beademing alleen naar beneden, met aandacht naar het perineum.
Er ontstaat steeds meer ruimte in de buik. Later, als deze basis geen actieve begeleiding meer nodig heeft,
maar min of meer vanzelf gaat, kan het lichaam van onderaf steeds verder worden gevuld met lucht. Vanaf het middenrif
kun je voelen hoe je meer naar achteren, naar opzij en volledig gevuld wordt en
uitzet, tot je je steeds vollediger en volledig hebt uitgezet en gevuld.


De HHP breathing excercise is een techniek die zou kunnen activeren. 's avonds zou je, om tot rust te komen en 
je ademhaling te kalmeren, evengoed de oefening kunnen doen zonder de adem in te houden. Er zijn geen voorschriften,
dit is alleen bedoeld om een ontwikkeling op gang te brengen.


Het schema
----------

2 maal 10 minuten overdag beoefenen
Opbouw: begin met 1 week 6 tellen inademen, 6 tellen uitademen
Dan een week 6 tellen inademen, 6 tellen uitademen, 12 seconden vasthouden in de buik (niet in de borst of keel)
Dan per week doorbouwen van 12 seconden vashouden naar 20 en 30

Het schema bestaat uit een aantal niveaus die onderweg bereikt worden als het
ritme min of meer continue op dat niveau is:

6-6-30, 9-9-30, 6-6-60, 9-9-60
[6 in - 6 uit, 30 inhouden, ..]

Het schema is natuurlijk niet leidend, maar kan een idee geven van het bereik. 
66-30 is het eerste mijlpaal en is al een heel mooi resultaat.

Speel met de duur waarop je je adem vasthoudt. Het idee is om het lichaam
enigszins in crisis te brengen, om je lichaam ertoe te brengen de processen van
opname en uitgifte efficienter te doen, zodat het aandeel van die processen in
het ademhalingsritme ook bijdragen aan een rustiger ritme. Een beetje uitdagen is goed,
maar teveel is heftig. Dat kan het ook moeilijker maken om deze praktijk
langdurig te doen op een manier waar je je goed bij voelt. Dus wees ook lief voor
jezelf.

Je lichaam geeft aan wanneer de adempauze genoeg is. De oefening is ook een oefening in luisteren naar je lichaam.
Dus experimenteer met de duur en de signalen van je lichaam. En oefen het luisteren.

Het zou goed kunnen dat de oefening in de ochtend beter gaat dan 's middags, omdat je 's ochtends nog meer in rust bent. 
Het helpt om voor de oefening jezelf in een ontspannen toestand te brengen. Hoe meer hoe beter. 
Er zijn technieken om jezelf of een ander versneld in delta staat te krijgen, maar alles wat je helpt is goed.


Verdieping
----------

Organen
-------

Als je eenmaal de beoefening een beetje op de rails hebt en je voelt ruimte om aandacht te besteden,
dan kun je naar je organen ademen. Een sessie je nieren, een andere je longen, je lever, dikke darm, dunne darm, galblaas, hart
Gaandeweg kijken of je de organen steeds beter kunt voelen.


Aanvullend materiaal
--------------------

Diafragma
---------
De soeplesse van je diafragma wordt door de oefening bevorderd. Aanvullend zou je voor je begint, terwijl je zit of staat,  een aantal keren hard "Chi", "chi", "chi" kunnen roepen, waarbij je de lucht hard met je buik uitstoot.


